#include <stdio.h>
#include <mosquitto.h>

int main(){
	int rc;
	struct mosquitto * mosq;
	//Инициализация библиотеки
	mosquitto_lib_init();
	//Создание нового клиента mosquitto
	mosq = mosquitto_new("publisher-test", true, NULL);
	//Подключение к брокеру MQTT
	rc = mosquitto_connect(mosq, "localhost", 1883, 60);
	//Проверка подключения
	if(rc != 0){
		printf("Client could not connect to broker! Error Code: %d\n", rc);
		mosquitto_destroy(mosq);
		return -1;
	}
	printf("Complete connect to broker!\n");
	//Публикация в топик
	mosquitto_publish(mosq, NULL, "test/tl", 6, "Hello", 0, false);
	//Отключение и уничтожение клиента
	mosquitto_disconnect(mosq);
	mosquitto_destroy(mosq);
	mosquitto_lib_cleanup();
	return 0;
}
