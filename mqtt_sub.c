#include <stdio.h>
#include <stdlib.h>

#include <mosquitto.h>

//Функция подписки на топик
void on_connect(struct mosquitto *mosq, void *obj, int rc) {
	printf("ID: %d\n", * (int *) obj);
	if(rc) {
		printf("Error with result code: %d\n", rc);
		exit(-1);
	}
	mosquitto_subscribe(mosq, NULL, "test/tl", 0);
}

//Вывод сообщения топика
void on_message(struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg) {
	printf("New message with topic %s: %s\n", msg->topic, (char *) msg->payload);
}

int main() {
	int rc, id=12;

	mosquitto_lib_init();

	struct mosquitto *mosq;
	//Создание нового клиента mosquitto
	mosq = mosquitto_new("subscribe-test", true, &id);
	//Обратный вызов для соединения
	mosquitto_connect_callback_set(mosq, on_connect);
	//Обратный вызов для сообщения
	mosquitto_message_callback_set(mosq, on_message);
	//Подключение к брокеру
	rc = mosquitto_connect(mosq, "localhost", 1883, 10);
	//Проверка подключения
	if(rc) {
		printf("Could not connect to Broker with return code %d\n", rc);
		return -1;
	}
	//Цикл для клиента. Служит для поддержки связи между клиентом и брокером.
	mosquitto_loop_start(mosq);
	printf("Press Enter to quit...\n");
	getchar();
	mosquitto_loop_stop(mosq, true);
	//Отключение и уничтожение клиента
	mosquitto_disconnect(mosq);
	mosquitto_destroy(mosq);
	mosquitto_lib_cleanup();

	return 0;
}
